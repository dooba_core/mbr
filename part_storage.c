/* Dooba SDK
 * MBR (Master Boot Record) Partition Format Driver
 */

// External Includes
#include <string.h>

// Internal Includes
#include "part_storage.h"

// Initialize Partition as Storage Device
void mbr_part_storage_init(struct mbr_part_storage *pst, struct storage *bst, uint16_t bst_sector_size, struct mbr_part *part, uint8_t pidx)
{
	// Check Start / Size
	if((part->lba_start == 0) || (part->sectors == 0))									{ return; }

	// Setup Partition Storage
	pst->bst = bst;
	pst->ptype = part->ptype;
	pst->base = (uint64_t)(part->lba_start) * bst_sector_size;
	pst->pidx = pidx;

	// Initialize Storage
	storage_init_dev(&(pst->st), pst, (uint64_t)(part->sectors) * bst_sector_size, (storage_io_t)mbr_part_storage_read, (storage_io_t)mbr_part_storage_write, (storage_sync_t)mbr_part_storage_sync, "%t.part.%i", bst->name, bst->name_len, pidx);
	storage_register(&(pst->st));
}

// Read Data
uint8_t mbr_part_storage_read(struct storage *s, struct mbr_part_storage *pst, uint64_t addr, void *data, uint16_t size)
{
	// Read
	return storage_read(pst->bst, pst->base + addr, data, size);
}

// Write Data
uint8_t mbr_part_storage_write(struct storage *s, struct mbr_part_storage *pst, uint64_t addr, void *data, uint16_t size)
{
	// Write
	return storage_write(pst->bst, pst->base + addr, data, size);
}

// Synchronize
uint8_t mbr_part_storage_sync(struct storage *s, struct mbr_part_storage *pst)
{
	// Sync
	return storage_sync(pst->bst);
}
