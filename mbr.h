/* Dooba SDK
 * MBR (Master Boot Record) Partition Format Driver
 */

#ifndef	__MBR_H
#define	__MBR_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <storage/storage.h>

// Internal Includes
#include "part.h"

// Bootstrap Code Size
#define	MBR_BOOTSTRAP_SIZE						446

// Partition Table Size
#define	MBR_PARTITIONS							4

// Boot Signature
#define	MBR_BOOT_SIGNATURE						0xaa55

// Compute Partition Entry Address
#define	mbr_entry_addr(idx)						(MBR_BOOTSTRAP_SIZE + (idx * sizeof(struct mbr_part)))

// Get / Set Partition
#define	mbr_get_part(st, idx, part)				storage_read(st, mbr_entry_addr(idx), part, sizeof(struct mbr_part))
#define	mbr_set_part(st, idx, part)				storage_write(st, mbr_entry_addr(idx), part, sizeof(struct mbr_part))

// Master Boot Record Structure
struct mbr
{
	// Bootstrap Code
	uint8_t bootstrap[MBR_BOOTSTRAP_SIZE];

	// Partition Table
	struct mbr_part partitions[MBR_PARTITIONS];

	// Signature
	uint16_t signature;
};

#endif
