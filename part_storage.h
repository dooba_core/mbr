/* Dooba SDK
 * MBR (Master Boot Record) Partition Format Driver
 */

#ifndef	__MBR_PART_STORAGE_H
#define	__MBR_PART_STORAGE_H

// External Includes
#include <stdint.h>
#include <storage/storage.h>

// Internal Includes
#include "mbr.h"
#include "part.h"

// MBR Partition Storage Structure
struct mbr_part_storage
{
	// Storage
	struct storage st;

	// Backend (Underlying Storage Device)
	struct storage *bst;

	// Partition Type
	uint8_t ptype;

	// Partition Index
	uint8_t pidx;

	// Base Address
	uint64_t base;
};

// Initialize Partition as Storage Device
extern void mbr_part_storage_init(struct mbr_part_storage *pst, struct storage *bst, uint16_t bst_sector_size, struct mbr_part *part, uint8_t pidx);

// Read Data
extern uint8_t mbr_part_storage_read(struct storage *s, struct mbr_part_storage *pst, uint64_t addr, void *data, uint16_t size);

// Write Data
extern uint8_t mbr_part_storage_write(struct storage *s, struct mbr_part_storage *pst, uint64_t addr, void *data, uint16_t size);

// Synchronize
extern uint8_t mbr_part_storage_sync(struct storage *s, struct mbr_part_storage *pst);

#endif
