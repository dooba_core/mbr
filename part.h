/* Dooba SDK
 * MBR (Master Boot Record) Partition Format Driver
 */

#ifndef	__MBR_PART_H
#define	__MBR_PART_H

// External Includes
#include <stdint.h>

// CHS Address Size
#define	MBR_PART_CHS_ADDR_SIZE				3

// Partition Structure
struct mbr_part
{
	// Status
	uint8_t status;

	// CHS Address of First Sector
	uint8_t chs_start[MBR_PART_CHS_ADDR_SIZE];

	// Partition Type
	uint8_t ptype;

	// CHS Address of Last Sector
	uint8_t chs_last[MBR_PART_CHS_ADDR_SIZE];

	// LBA Address of First Sector
	uint32_t lba_start;

	// Number of Sectors
	uint32_t sectors;
};

#endif
