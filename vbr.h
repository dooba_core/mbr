/* Dooba SDK
 * MBR (Master Boot Record) Partition Format Driver
 */

#ifndef	__MBR_VBR_H
#define	__MBR_VBR_H

// External Includes
#include <stdint.h>

// Jump Instruction Size
#define	MBR_VBR_JMP_SIZE				3

// OEM Name Length
#define	MBR_VBR_OEM_NAME_LEN			8

// Volume Boot Record Structure
struct mbr_vbr
{
	// Jump Instruction
	uint8_t jmp[MBR_VBR_JMP_SIZE];

	// OEM Name
	char oem_name[MBR_VBR_OEM_NAME_LEN];
};

#endif
